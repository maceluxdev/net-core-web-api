namespace web_api_tutorial.Models
{
     // <T> actual type of the data we will want to send back
    public class ServiceResponse <T>
    {
        public T Data {get; set;}
        public bool Success {get; set;} = true;
        public string Message {get; set;} = null;
    }
}