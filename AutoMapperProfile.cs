using AutoMapper;
using web_api_tutorial.Dtos.Character;
using web_api_tutorial.Models;

namespace web_api_tutorial
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Character, GetCharacterDto>();
            CreateMap<AddCharacterDto, Character>();
        }
    }
}